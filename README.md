### ftpbackup

Ftpbackup backups your data from a Debian server to a ftp dedicated space.

#### What ftpbackup is going to save

Ftpbackup only saves:

- a list of all the installed Debian packages
- all your mysql databases in a dump
- /etc /var /home /opt /usr/local/bin directories

See below to exclude directories or files from the saved directories.

#### Configure Ftpbackup

In order to configure Ftpbackup, create the file /etc/ftpbackup/ftpbackup.conf:

        BACKUPHOME=/var/lib/backups
        FTPUSER=ftpuser
        FTPPASS=R34lLyD1fF1cuLt
        FTPSERVER=my.ftp.server
        MYSQLUSER=backup
        MYSQLPASS=An0th3rD1fF1culTP4s5w0rD
        MYSQLHOST=localhost
        ADMINEMAIL=admin@bigcompany.com
        # retention period (in days)
        RETENTION=5
        EXCLUDES="--exclude=/home/whocares --exclude=/var/cache/dontcare"

* Do not save some directories or files

The parameter EXCLUDES allows you to define excludes from the tar file. This excludes should be subdirectories of the saved repertories or files inside the saved directories.
For constitency, only use absolute paths.

#### Launch Ftpbackup

        $ /path/to/ftpbackup

### Authors

Carl Chenet <chaica@ohmytux.com>

### License

This software comes under the terms of the GPLv3+. See the LICENSE file for the complete text of the license.
